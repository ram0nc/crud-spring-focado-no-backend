package maven.testecasadocodigo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import maven.testecasadocodigo.dao.ProdutoDAO;
import maven.testecasadocodigo.model.Produto;

@Controller
public class HomeController {

	@Autowired
	private ProdutoDAO produtoDAO;
	
	
	@RequestMapping("/")
	@Cacheable(value = "produtoHome")
	public ModelAndView index() {
		List<Produto> produtos =  produtoDAO.listar();
		ModelAndView mav = new ModelAndView("home");
		mav.addObject("produtos", produtos);
		
		return mav;
	}
}
