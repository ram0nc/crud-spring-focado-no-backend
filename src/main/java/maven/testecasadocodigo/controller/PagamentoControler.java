package maven.testecasadocodigo.controller;

import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import maven.testecasadocodigo.model.CarrinhoCompras;
import maven.testecasadocodigo.model.DadosPagamento;
import maven.testecasadocodigo.model.Usuario;

@RequestMapping("/pagamento")
@Controller
public class PagamentoControler {

	
	@Autowired
	private CarrinhoCompras carrinho;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private MailSender sender;

	@RequestMapping(value = "/finalizar", method = RequestMethod.POST)
	public Callable<ModelAndView> finalizar(@AuthenticationPrincipal Usuario usuario, 
			RedirectAttributes model) {
		return () -> {
			
		try {
			String uri = "http://book-payment.herokuapp.com/payment";
			String response = restTemplate.postForObject(uri, new DadosPagamento(carrinho.getTotal()), String.class);
			
			System.out.println(response);
			enviaEmailCompraProduto(usuario);
			model.addFlashAttribute("sucesso",  response);
			
			
		}catch (HttpClientErrorException e) {
			model.addFlashAttribute("falha",  "valor maior que o permitido");
		}
		
		return new ModelAndView("redirect:/produtos");
		};
	}

	private void enviaEmailCompraProduto(Usuario usuario) {
		
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject("Compra finalizada com sucesso");
		email.setTo("ramoncgusmao@gmail.com");
		email.setText("Compra aprovada com sucesso no valor de " + carrinho.getTotal());
		email.setFrom("ramon.gatao@gmail.com");
		
		sender.send(email);
	}
	
	
	
}
