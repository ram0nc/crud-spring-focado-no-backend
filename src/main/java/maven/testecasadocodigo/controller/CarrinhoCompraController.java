package maven.testecasadocodigo.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import maven.testecasadocodigo.dao.ProdutoDAO;
import maven.testecasadocodigo.model.CarrinhoCompras;
import maven.testecasadocodigo.model.CarrinhoItem;
import maven.testecasadocodigo.model.Produto;
import maven.testecasadocodigo.model.Tipo;

@Controller
@RequestMapping("/carrinho")
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
public class CarrinhoCompraController {

	@Autowired
	private ProdutoDAO produtoDAO;
	
	@Autowired
	private CarrinhoCompras carrinho;

	@RequestMapping("/add")
	public ModelAndView add(int produtoId, Tipo tipoPreco) {
		
		ModelAndView modelAndView = new ModelAndView("redirect:/carrinho");
		CarrinhoItem carrinhoItem = criarItem(produtoId, tipoPreco);
		
		carrinho.add(carrinhoItem);
		//System.out.println(carrinhoItem);
		return modelAndView;
	}

	private CarrinhoItem criarItem(int produtoId, Tipo tipoPreco) {
		Produto produto =  produtoDAO.find(produtoId);
		System.out.println(produto);
		CarrinhoItem carrinhoItem = new CarrinhoItem(produto, tipoPreco);
		return carrinhoItem;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView itens(){
		
		
		return new ModelAndView("carrinho/itens");
	}
	
	
	@RequestMapping("/remover")
	public ModelAndView remover(Integer produtoId, Tipo tipoPreco) {
		
		System.out.println("prod :" + produtoId + "tipo: " + tipoPreco);
		carrinho.remover(produtoId,tipoPreco);
		return new ModelAndView("redirect:/carrinho");
	}
	
}
