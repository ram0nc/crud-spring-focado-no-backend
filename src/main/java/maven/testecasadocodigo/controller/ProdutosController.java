package maven.testecasadocodigo.controller;

import java.util.List;

import javax.persistence.NoResultException;
import javax.servlet.annotation.MultipartConfig;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import maven.testecasadocodigo.dao.ProdutoDAO;
import maven.testecasadocodigo.infra.FileSaver;
import maven.testecasadocodigo.model.Produto;
import maven.testecasadocodigo.model.Tipo;
import maven.testecasadocodigo.validation.ProdutoValidation;

@Controller
@RequestMapping("/produtos")
public class ProdutosController {

	@Autowired
	private ProdutoDAO produtoDao;
	
	@Autowired
	private FileSaver fileSaver;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProdutoValidation() );
	}
	@RequestMapping("/form")
	public ModelAndView form(Produto produto) {
		
		ModelAndView mav =  new ModelAndView("produtos/form");
		mav.addObject("Tipos",Tipo.values());
		
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST)
	@CacheEvict(value="produtoHome", allEntries = true)
	public ModelAndView gravar(MultipartFile sumario, @Valid Produto produto, BindingResult result, RedirectAttributes redirectAttributes) {
		
		System.out.println(sumario.getOriginalFilename());
		
		
		if(result.hasErrors()) {
			return form(produto);
		}
		produtoDao.gravar(produto);
		 String path = fileSaver.write("arquivos-sumario", sumario);
		 produto.setSumarioPath(path);
		 redirectAttributes.addFlashAttribute("sucesso", "Produto cadastrado com sucesso");
		return new ModelAndView("redirect:produtos");
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView listar() {
		
		List<Produto> listaProdutos =  produtoDao.listar();
		ModelAndView mav = new ModelAndView("produtos/lista");
		mav.addObject("produtos",listaProdutos);
		return mav;
	}
	
	@RequestMapping("/detalhe/{id}")
	public ModelAndView detalhe(@PathVariable("id") Integer id) {
		ModelAndView modelAndView = new ModelAndView("produtos/detalhe");
		Produto produto = produtoDao.find(id);
		
		modelAndView.addObject("produto", produto);

		return modelAndView;
		
	}

	
}
