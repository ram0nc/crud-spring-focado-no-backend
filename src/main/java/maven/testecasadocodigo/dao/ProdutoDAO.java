package maven.testecasadocodigo.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import maven.testecasadocodigo.model.Produto;
import maven.testecasadocodigo.model.Tipo;

@Repository
@Transactional
public class ProdutoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void gravar(Produto produto) {

		manager.persist(produto);
	}

	public List<Produto> listar() {

		return manager.createQuery("select distinct(p) from Produto p join fetch p.precos", Produto.class).getResultList();
	}

	public Produto find(Integer id) {
		// TODO Auto-generated method stub
		String query = "select distinct(p) from Produto p" + " join fetch p.precos preco where p.id = :id";
		return manager.createQuery(query, Produto.class)
						.setParameter("id", id)
						.getSingleResult();
	}


	public BigDecimal somaPrecoPorTipo(Tipo tipoPreco) {
		 TypedQuery<BigDecimal> query = manager.createQuery("select sum(preco.valor) from Produto p "
				+ "join p.precos preco where preco.tipoPreco = :tPreco", BigDecimal.class);
		query.setParameter("tPreco", tipoPreco);
		
	return query.getSingleResult();
	}
	

}
