package maven.testecasadocodigo.model;

import java.math.BigDecimal;

import javax.persistence.Embeddable;

@Embeddable
public class Preco {

	private BigDecimal valor;
	private Tipo tipoPreco;
	
	
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public Tipo getTipoPreco() {
		return tipoPreco;
	}
	public void setTipoPreco(Tipo tipopreco) {
		this.tipoPreco = tipopreco;
	}
	@Override
	public String toString() {
		return tipoPreco.name() +" - " + getValor();
	}
	
	
	
	
}
