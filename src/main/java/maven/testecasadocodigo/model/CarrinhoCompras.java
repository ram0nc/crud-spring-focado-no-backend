package maven.testecasadocodigo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, 
proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CarrinhoCompras implements Serializable{

	private Map<CarrinhoItem, Integer> itens = new LinkedHashMap<CarrinhoItem, Integer>();

	public void add(CarrinhoItem item) {
		
		itens.put(item, getQuantidade(item) + 1);
		
		
	}

	public Integer getQuantidade(CarrinhoItem item) {
		if(! itens.containsKey(item)) {
			itens.put(item, 0);
		}
		
		System.out.println(itens.get(item));
		return itens.get(item);
		
	}

	public int getQuantidade() {
		return itens.values().stream().reduce(0, 
				(proximo, acumulador) -> proximo + acumulador);
	}

	public Collection<CarrinhoItem> getItens() {
		return itens.keySet();
	}
	
	public BigDecimal getTotal() {
		BigDecimal total = BigDecimal.ZERO;
		
		for (CarrinhoItem item : itens.keySet()) {
			total = total.add(getTotal(item));
		}
		
		return total;
	}
	
	public BigDecimal getTotal(CarrinhoItem item){
		System.out.println(item);
		return item.getTotal(getQuantidade(item));
		
	}

	public void remover(Integer produtoId, Tipo tipoPreco) {
		Produto produto = new Produto();
		produto.setId(produtoId);
		itens.remove(new CarrinhoItem(produto, tipoPreco));
		
	}

	
	
}
