<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<c:url value="/resources/" var="cssPath" />
<link rel="stylesheet" type="text/css" media="all"
	href="${cssPath}css/bootstrap.min.css">
<script type="text/javascript" src="${cssPath}js/jquery.min.js"></script>
<script type="text/javascript" src="${cssPath}js/bootstrap.min.js"></script>
<title>livros de java</title>
</head>
<body>
	
	<div class="container p-5">
	<h1> Login da casa do codigo</h1>
		<form:form  servletRelativeAction="/login" method="post">
			<div class="form-group">
				<label>E-mail</label>
				<input name="username" type="text" class="form-control"/>
				
			</div>
			<div class="form-group">
				<label>Senha</label>
				<input name="password" type="password" Class="form-control"/>
			
			</div>
		

			<button type="submit" class="btn btn-success p-4">Logar</button>

		</form:form>
	</div>
</body>
</html>