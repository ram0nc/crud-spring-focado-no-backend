package maven.testecasadocodigo.dao;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import maven.testecasadocodigo.builders.ProdutoBuilder;
import maven.testecasadocodigo.conf.DataSourceConfigurationTest;
import maven.testecasadocodigo.conf.JPAConfiguration;
import maven.testecasadocodigo.model.Produto;
import maven.testecasadocodigo.model.Tipo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={JPAConfiguration.class, ProdutoDAO.class, DataSourceConfigurationTest.class})
@ActiveProfiles("test")
public class ProdutoDAOTest {

	@Autowired
	private ProdutoDAO produtoDAO;
	
	@Test
	@Transactional
	public void deveSomarTodosPrecosPorTipoLivro() {
	
		List<Produto> livrosImpressos = ProdutoBuilder.newProduto(Tipo.IMPRESSO, BigDecimal.TEN).more(5).buildAll();
		
		List<Produto> livrosEbook = ProdutoBuilder.newProduto(Tipo.EBOOK, BigDecimal.TEN).more(5).buildAll();
		
		livrosImpressos.stream().forEach(produtoDAO::gravar);
		livrosEbook.stream().forEach(produtoDAO::gravar);
		
		BigDecimal valor = produtoDAO.somaPrecoPorTipo(Tipo.EBOOK);
		Assert.assertEquals(new BigDecimal(60).setScale(2), valor);
	}
}
